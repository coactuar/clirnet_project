-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 12:53 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CLIRNet_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_emailid` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_emailid`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Diwakar_Sharma', '', 'heelo', '2021-07-05 19:32:12', 'pollsdemo01', 1, 0),
(2, 'Pawan', '', 'sss', '2021-07-05 19:39:09', 'pollsdemo01', 0, 0),
(3, 'Diwakar_Sharma', '', 'sss', '2021-07-05 20:16:07', 'pollsdemo01', 0, 0),
(4, 'Diwakar_Sharma', '', 'sssss', '2021-07-05 20:17:55', 'pollsdemo01', 0, 0),
(5, 'Diwakar_Sharma', '', 'Hello', '2021-07-05 20:19:32', 'pollsdemo01', 0, 0),
(6, 'Diwakar_Sharma', '', 'Hello', '2021-07-05 20:19:54', 'pollsdemo01', 0, 0),
(7, 'ban', '', 'ss', '2021-07-05 20:32:21', 'pollsdemo01', 0, 0),
(8, 'ban', '', 'ss', '2021-07-05 20:32:22', 'pollsdemo01', 0, 0),
(9, 'rajeshawar', '', 'ss', '2021-07-05 20:57:02', 'pollsdemo01', 0, 0),
(10, 'Rakshith_Shetty', '408', 'its pushing from server', '2021-07-06 12:39:19', 'pollsdemo01', 0, 0),
(11, 'pawan', '408', 'ss', '2021-07-06 13:02:41', 'pollsdemo01', 0, 0),
(12, 'pandu', '408', 'hello', '2021-07-06 13:04:58', 'pollsdemo01', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `joining_date` datetime DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) DEFAULT NULL COMMENT '1=loggedin',
  `eventname` varchar(255) DEFAULT NULL,
  `user_id` varchar(96) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `user_id`) VALUES
(1, 'Diwakar_Sharma', '2021-07-05 19:20:43', '2021-07-05 19:20:43', '2021-07-05 19:21:13', 1, 'pollsdemo01', '408'),
(2, 'Diwakar_Sharma', '2021-07-05 19:28:19', '2021-07-05 19:28:19', '2021-07-05 19:28:49', 1, 'pollsdemo01', '408'),
(3, 'Rakshith_shetty', '2021-07-05 19:28:47', '2021-07-05 19:28:47', '2021-07-05 19:29:17', 1, 'pollsdemo01', '438'),
(4, 'Rakshith_shetty', '2021-07-05 19:34:48', '2021-07-05 19:34:48', '2021-07-05 19:35:18', 1, 'pollsdemo01', '438'),
(5, 'Rakshith_shetty', '2021-07-05 19:35:04', '2021-07-05 19:35:04', '2021-07-05 19:35:34', 1, 'pollsdemo01', '438'),
(6, 'Rakshith_shetty', '2021-07-05 19:35:13', '2021-07-05 19:35:13', '2021-07-05 19:35:43', 1, 'pollsdemo01', '438'),
(7, 'Rakshith_shetty', '2021-07-05 19:35:44', '2021-07-05 19:35:44', '2021-07-05 19:36:14', 1, 'pollsdemo01', '438'),
(8, 'Rakshith_shetty', '2021-07-05 19:36:10', '2021-07-05 19:36:10', '2021-07-05 19:36:40', 1, 'pollsdemo01', '438'),
(9, 'Rakshith_shetty', '2021-07-05 19:36:26', '2021-07-05 19:36:26', '2021-07-05 19:36:56', 1, 'pollsdemo01', '438'),
(10, 'Rakshith_shetty', '2021-07-05 19:36:59', '2021-07-05 19:36:59', '2021-07-05 19:37:29', 1, 'pollsdemo01', '438'),
(11, 'Rakshith_shetty', '2021-07-05 19:37:20', '2021-07-05 19:37:20', '2021-07-05 19:37:50', 1, 'pollsdemo01', '438'),
(12, 'Rakshith_shetty', '2021-07-05 19:37:30', '2021-07-05 19:37:30', '2021-07-05 19:38:00', 1, 'pollsdemo01', '438'),
(13, 'Rakshith_shetty', '2021-07-05 19:37:52', '2021-07-05 19:37:52', '2021-07-05 19:38:22', 1, 'pollsdemo01', '438'),
(14, 'Pawan', '2021-07-05 19:38:20', '2021-07-05 19:38:20', '2021-07-05 19:38:50', 1, 'pollsdemo01', '4568'),
(15, 'Pawan', '2021-07-05 19:39:57', '2021-07-05 19:39:57', '2021-07-05 19:40:27', 1, 'pollsdemo01', '4568'),
(16, 'Diwakar_Sharma', '2021-07-05 20:02:51', '2021-07-05 20:02:51', '2021-07-05 20:03:21', 1, 'pollsdemo01', '408'),
(17, 'Pawan', '2021-07-05 20:04:13', '2021-07-05 20:04:13', '2021-07-05 20:04:43', 1, 'pollsdemo01', '4568'),
(18, 'Diwakar_Sharma', '2021-07-05 20:11:37', '2021-07-05 20:11:37', '2021-07-05 20:12:07', 1, 'pollsdemo01', '408'),
(19, 'Diwakar_Sharma', '2021-07-05 20:15:31', '2021-07-05 20:15:31', '2021-07-05 20:16:01', 1, 'pollsdemo01', '408'),
(20, 'Neeraj', '2021-07-05 20:16:54', '2021-07-05 20:16:54', '2021-07-05 20:17:24', 1, 'pollsdemo01', '450'),
(21, 'Diwakar_Sharma', '2021-07-05 20:17:17', '2021-07-05 20:17:17', '2021-07-05 20:17:47', 1, 'pollsdemo01', '408'),
(22, 'Diwakar_Sharma', '2021-07-05 20:18:31', '2021-07-05 20:18:31', '2021-07-05 20:19:01', 1, 'pollsdemo01', '408'),
(23, 'Diwakar_Sharma', '2021-07-05 20:18:32', '2021-07-05 20:18:32', '2021-07-05 20:19:02', 1, 'pollsdemo01', '408'),
(24, 'Diwakar_Sharma', '2021-07-05 20:19:22', '2021-07-05 20:19:22', '2021-07-05 20:19:52', 1, 'pollsdemo01', '408'),
(25, 'Diwakar_Sharma', '2021-07-05 20:21:01', '2021-07-05 20:21:01', '2021-07-05 20:21:31', 1, 'pollsdemo01', '408'),
(26, 'Diwakar_Sharma', '2021-07-05 20:21:08', '2021-07-05 20:21:08', '2021-07-05 20:21:38', 1, 'pollsdemo01', '408'),
(27, 'Diwakar_Sharma', '2021-07-05 20:22:30', '2021-07-05 20:22:30', '2021-07-05 20:23:00', 1, 'pollsdemo01', '408'),
(28, 'Diwakar_Sharma', '2021-07-05 20:22:37', '2021-07-05 20:22:37', '2021-07-05 20:23:07', 1, 'pollsdemo01', '408'),
(29, 'Rakshith_Shetty', '2021-07-06 12:38:59', '2021-07-06 12:38:59', '2021-07-06 12:38:59', 1, 'pollsdemo01', '408'),
(30, 'Rakshith_Shetty', '2021-07-06 12:39:06', '2021-07-06 12:39:06', '2021-07-06 12:39:06', 1, 'pollsdemo01', '408'),
(31, 'mahesh_patil', '2021-07-06 12:42:52', '2021-07-06 12:42:52', '2021-07-06 12:42:52', 1, 'pollsdemo01', '408'),
(32, '', '2021-07-06 12:46:17', '2021-07-06 12:46:17', '2021-07-06 12:46:17', 1, 'pollsdemo01', ''),
(33, '', '2021-07-06 12:46:38', '2021-07-06 12:46:38', '2021-07-06 12:46:38', 1, 'pollsdemo01', ''),
(34, '', '2021-07-06 12:46:41', '2021-07-06 12:46:41', '2021-07-06 12:46:41', 1, 'pollsdemo01', ''),
(35, 'mahesh_sharma', '2021-07-06 12:47:26', '2021-07-06 12:47:26', '2021-07-06 12:47:26', 1, 'pollsdemo01', '408'),
(36, 'mahesh_sharma', '2021-07-06 12:47:41', '2021-07-06 12:47:41', '2021-07-06 12:47:41', 1, 'pollsdemo01', '408'),
(37, 'neeraj_reddy', '2021-07-06 12:48:59', '2021-07-06 12:48:59', '2021-07-06 12:48:59', 1, 'pollsdemo01', '408'),
(38, 'Diwakar_Sharma', '2021-07-06 12:49:32', '2021-07-06 12:49:32', '2021-07-06 12:49:32', 1, 'pollsdemo01', '408'),
(39, 'Diwakar_Sharma', '2021-07-06 12:56:05', '2021-07-06 12:56:05', '2021-07-06 12:56:05', 1, 'pollsdemo01', '408'),
(40, 'Diwakar_Sharma', '2021-07-06 12:58:06', '2021-07-06 12:58:06', '2021-07-06 12:58:06', 1, 'pollsdemo01', '408'),
(41, 'darshan', '2021-07-06 12:58:23', '2021-07-06 12:58:23', '2021-07-06 12:58:23', 1, 'pollsdemo01', '408'),
(42, 'Diwakar_Sharma', '2021-07-06 13:02:28', '2021-07-06 13:02:28', '2021-07-06 13:02:28', 1, 'pollsdemo01', '408'),
(43, 'pawan', '2021-07-06 13:02:36', '2021-07-06 13:02:36', '2021-07-06 13:02:36', 1, 'pollsdemo01', '408'),
(44, 'Diwakar_Sharma', '2021-07-06 13:04:20', '2021-07-06 13:04:20', '2021-07-06 13:04:20', 1, 'pollsdemo01', '408'),
(45, 'pandu', '2021-07-06 13:04:47', '2021-07-06 13:04:47', '2021-07-06 13:04:47', 1, 'pollsdemo01', '408'),
(46, 'pandu', '2021-07-06 13:09:56', '2021-07-06 13:09:56', '2021-07-06 13:09:56', 1, 'pollsdemo01', '408'),
(47, 'pawan', '2021-07-06 13:12:40', '2021-07-06 13:12:40', '2021-07-06 13:12:40', 1, 'pollsdemo01', '408'),
(48, 'Diwakar_Sharma', '2021-07-06 13:13:29', '2021-07-06 13:13:29', '2021-07-06 13:13:29', 1, 'pollsdemo01', '408'),
(49, '', '2021-07-06 13:13:37', '2021-07-06 13:13:37', '2021-07-06 13:13:37', 1, 'pollsdemo01', ''),
(50, 'Diwakar_Sharma', '2021-07-06 13:28:24', '2021-07-06 13:28:24', '2021-07-06 13:28:24', 1, 'pollsdemo01', '408'),
(51, 'neerajreddy', '2021-07-06 13:28:53', '2021-07-06 13:28:53', '2021-07-06 13:28:53', 1, 'pollsdemo01', '408'),
(52, 'Diwakar_Sharma', '2021-07-06 13:35:49', '2021-07-06 13:35:49', '2021-07-06 13:35:49', 1, 'pollsdemo01', '408'),
(53, 'Diwakar_Sharma', '2021-07-06 13:35:58', '2021-07-06 13:35:58', '2021-07-06 13:35:58', 1, 'pollsdemo01', '408'),
(54, 'Diwakar_Sharma', '2021-07-06 13:36:10', '2021-07-06 13:36:10', '2021-07-06 13:36:10', 1, 'pollsdemo01', '408'),
(55, 'mahesh', '2021-07-06 13:36:32', '2021-07-06 13:36:32', '2021-07-06 13:36:32', 1, 'pollsdemo01', '408'),
(56, 'Diwakar_Sharma', '2021-07-06 14:14:39', '2021-07-06 14:14:39', '2021-07-06 14:14:39', 1, 'pollsdemo01', '408'),
(57, 'Diwakar_Sharma', '2021-07-06 15:19:04', '2021-07-06 15:19:04', '2021-07-06 15:19:04', 1, 'pollsdemo01', '408'),
(58, '', '2021-07-26 15:12:35', '2021-07-26 15:12:35', '2021-07-26 15:12:35', 1, 'pollsdemo01', ''),
(59, 'Diwakar_Sharma', '2021-07-26 15:12:35', '2021-07-26 15:12:35', '2021-07-26 15:12:35', 1, 'pollsdemo01', '408'),
(60, 'Diwakar_Sharma', '2021-07-26 16:27:59', '2021-07-26 16:27:59', '2021-07-26 16:27:59', 1, 'pollsdemo01', '408'),
(61, 'Diwakar_Sharma', '2021-07-26 16:28:11', '2021-07-26 16:28:11', '2021-07-26 16:28:11', 1, 'pollsdemo01', '408'),
(62, '', '2021-08-09 14:05:31', '2021-08-09 14:05:31', '2021-08-09 14:05:31', 1, 'pollsdemo01', ''),
(63, '', '2021-08-09 14:05:44', '2021-08-09 14:05:44', '2021-08-09 14:05:44', 1, 'pollsdemo01', ''),
(64, 'pawan', '2021-09-17 11:11:33', '2021-09-17 11:11:33', '2021-09-17 11:11:33', 1, 'pollsdemo01', 'awan32'),
(65, 'Rakshith_shetty', '2021-09-17 12:07:43', '2021-09-17 12:07:43', '2021-09-17 12:07:43', 1, 'pollsdemo01', '438');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
